// TDD - Unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
  describe("RGB to Hex conversion", () => {
    it("converts the basic colors", () => {
      const redHex = converter.rgbToHex(255, 0, 0); // red ff0000
      const greenHex = converter.rgbToHex(0, 255, 0); // green 00ff00
      const blueHex = converter.rgbToHex(0, 0, 255); // blue 0000ff

      expect(redHex).to.equal("ff0000");
      expect(greenHex).to.equal("00ff00");
      expect(blueHex).to.equal("0000ff");
    });
  });
  describe("Hex to RGB conversion", () => {
    it("converts the basic colors", () => {
      const redRgb = converter.hexToRgb("ff0000"); // red rgb(255,0,0)
      const greenRgb = converter.hexToRgb("00ff00"); // green rgb(0,255,0)
      const blueRgb = converter.hexToRgb("0000ff"); // blue rgb(0,0,255)

      expect(redRgb).to.equal("rgb(255,0,0)");
      expect(greenRgb).to.equal("rgb(0,255,0)");
      expect(blueRgb).to.equal("rgb(0,0,255)");
    });
  });
});
