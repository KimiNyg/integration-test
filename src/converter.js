/**
 * @param {string} hex
 */

const pad = (hex) => {
  return hex.length === 1 ? "0" + hex : hex;
};

module.exports = {
  rgbToHex: (red, green, blue) => {
    const redHex = red.toString(16); // 0-255 -> 0-ff
    const greenHex = green.toString(16);
    const blueHex = blue.toString(16);

    return pad(redHex) + pad(greenHex) + pad(blueHex);
  },
  hexToRgb: (hex) => {
    if (hex.length !== 6 && hex.length !== 3) {
      return "Only hex values with 3 or 6 digits allowed!";
    }
    if (hex.length === 3) {
      hex = hex.replace(/.{1,1}/g, "$&$&");
    }
    const rgb = hex.match(/.{1,2}/g);
    const rgbArray = [
      parseInt(rgb[0], 16),
      parseInt(rgb[1], 16),
      parseInt(rgb[2], 16),
    ];
    return "rgb(" + rgbArray[0] + "," + rgbArray[1] + "," + rgbArray[2] + ")";
  },
};
